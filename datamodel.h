#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QObject>
#include <QStringList>


class DataModel : public QObject
{
    Q_OBJECT
//    Q_PROPERTY(QStringList languageList READ languageList NOTIFY languageListUpdated )
public:
    DataModel(QObject *parent = nullptr);

signals:
    void languageListUpdated();

private:
    void createDB();
    void preload();


};

#endif // DATAMODEL_H
