#include "datamodel.h"
#include <QSqlDatabase>

#include <QDebug>


const QString INITIAL_SCRIPT =
        "CREATE TABLE IF NOT EXISTS \"tcl_language_code\" ( "
        "\"language_code_id\"	INTEGER PRIMARY KEY AUTOINCREMENT, "
        "\"code\"	TEXT NOT NULL, "
        "\"name\"	TEXT NOT NULL "
        ");"
        "INSERT INTO \"tcl_language_code\"(\"code\", \"name\") VALUES(\"ru\", \"Русский\"); "
        "INSERT INTO \"tcl_language_code\"(\"code\", \"name\") VALUES(\"ar\", \"Арабский\"); "
        "INSERT INTO \"tcl_language_code\"(\"code\", \"name\") VALUES(\"en\", \"Английский\"); "

        "CREATE TABLE IF NOT EXISTS \"tword\" ( "
        "\"tword_id\"	INTEGER PRIMARY KEY AUTOINCREMENT, "
        "\"word\"	TEXT NOT NULL, "
        "\"translate\"	TEXT NOT NULL, "
        "\"language_code_id\"	INTEGER NOT NULL, "
        "\"translate_language_code_id\"	INTEGER NOT NULL, "
        "FOREIGN KEY (language_code_id) REFERENCES tcl_language_code(language_code_id) "
        "); "

        "CREATE TABLE IF NOT EXISTS \"tpractice\" ( "
        "\"practice_id\"	INTEGER PRIMARY KEY AUTOINCREMENT, "
        "\"world_id\"	INTEGER NOT NULL, "
        "\"datetime\"	TEXT NOT NULL, "
        "FOREIGN KEY (world_id) REFERENCES Persons(PersonID) "
        "); ";


const QString DATABASE_NAME = "";



DataModel::DataModel(QObject *parent) : QObject(parent)
{
    for(auto i : INITIAL_SCRIPT.split(';'))
        qDebug() << i;
}

void DataModel::createDB()
{

}


